package com.pym.academyapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentRequest {
    private String name;
    private String gender;
    private Byte age;
    private String address;
}
