package com.pym.academyapi.controller;

import com.pym.academyapi.model.StudentRequest;
import com.pym.academyapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor //누군가가 꼭 필요(혼자X)
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/new")
    public String setStudent(@RequestBody StudentRequest request){
        studentService.setStudent(request);
        return "OK";
    }
}
